/* Without Promises */
asynchronousFunctionOne(function(value){
	somethingSync(value);
	asynchronousFunctionTwo(function(){
		somethingElseSync();
		asynchronousFunctionThree(function(){
			console.log("Done!");
		});
	});
});

/* With Promises */
Promise.try(function(){
	return asynchronousFunctionOne();
}).then(function(value){
	somethingSync(value);
	return asynchronousFunctionTwo();
}).then(function(){
	somethingElseSync();
	return asynchronousFunctionThree();
}).then(function(){
	console.log("Done!");
});
